package dao;

import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import collectionmetier.CollectionMetier;
import exceptions.UOException;
import model.Chambre;
import model.ConnexionBDD;

public class DAOChambre implements IDAO<Chambre> {

	// ---------------------------	
	//METHODE CREATE
	// ---------------------------
	public Chambre create(Chambre chambreACreer) {

		String req = "INSERT INTO CHAMBRE (NUCHAMBRE, NUCATCHAMBRE, NUHOTEL) VALUES (?, ?, ?)";

		try {
			PreparedStatement monPS = ConnexionBDD.getCxion().prepareStatement(req);

			monPS.setInt(1, chambreACreer.getNumero());
			monPS.setInt(2, chambreACreer.getNumeroCategorie());
			monPS.setInt(3, chambreACreer.getNum_hotel());

			monPS.executeUpdate();
			//Message d'erreur � mettre en place
		} catch (SQLException e) {
			System.err.println("La chambre existe d�ja!");
		}

		return chambreACreer;
	}

	// ---------------------------	
	//METHODE UPDATE
	// ---------------------------

	@Override
	public Chambre update(Chambre chambreAModifier) {

		// je v�rifie si la ligne que je desire supprimer existe	
		try {
			String req = "SELECT * FROM CHAMBRE WHERE NUHOTEL = " + chambreAModifier.getNum_hotel() + "AND NUCHAMBRE = " + chambreAModifier.getNumero();

			Statement monStatement = ConnexionBDD.getCxion().createStatement();

			ResultSet monRS = monStatement.executeQuery(req);

			if (! monRS.next()) {						
				System.err.println("la chambre n'existe pas !");

			}else{
				req = "SELECT * FROM CHAMBRE WHERE NUHOTEL = " + chambreAModifier.getNum_hotel() + "AND NUCHAMBRE = " + chambreAModifier.getNumero()+ "AND NUCATCHAMBRE = " + chambreAModifier.getNumeroCategorie();

				ResultSet monRS1 = monStatement.executeQuery(req);

				if (monRS1.next()) {

					System.err.println("il n y a pas de modification !");

				}else {

					ConnexionBDD.getCxion().createStatement().executeUpdate(
							"UPDATE CHAMBRE SET NUCATCHAMBRE = " + chambreAModifier.getNumeroCategorie() + "WHERE NUCHAMBRE = " + chambreAModifier.getNumero() + "AND NUHOTEL = " + chambreAModifier.getNum_hotel());
					System.out.println("Chambre modifi�e");
				}

			}

		}catch (SQLException e) {
			System.err.println("Saisie incorrecte.");
		}

		return chambreAModifier;
	}
	// ---------------------------	
	//METHODE DFELETE
	// ---------------------------
	@Override		
	//OK
	public void delete(Chambre chambreAsupprimer) {

		// je v�rifie si la ligne que je desire supprimer existe		
		try {
			String req = "SELECT * FROM CHAMBRE WHERE NUHOTEL = " + chambreAsupprimer.getNum_hotel() + "AND NUCHAMBRE = " + chambreAsupprimer.getNumero();

			Statement monStatement = ConnexionBDD.getCxion().createStatement();

			ResultSet monRS = monStatement.executeQuery(req);

			if (! monRS.next()) {						
				System.err.println("la chambre n'est pas l� !!!!");

			}else{
				ConnexionBDD.getCxion().createStatement().executeUpdate(
						"DELETE FROM CHAMBRE WHERE NUHOTEL = " + chambreAsupprimer.getNum_hotel() + "AND NUCHAMBRE = " + chambreAsupprimer.getNumero());
				System.out.println("Ligne supprim�e");				
			}

		}catch (SQLException e) {
			e.printStackTrace();
		}
	}

	// ---------------------------	
	//METHODE FIND BY ID
	// ---------------------------
	public Chambre findById(int idChambreARechercher, int idHotel) {

		Chambre laChambre = new Chambre();

		String req = "SELECT * FROM CHAMBRE WHERE NUCHAMBRE = ? AND NUHOTEL = ?";

		try {
			PreparedStatement monPS = ConnexionBDD.getCxion().prepareStatement(req);

			monPS.setInt(1, idChambreARechercher);
			monPS.setInt(2, idHotel);

			ResultSet monRS = monPS.executeQuery();
			
			if (! monRS.next()) {
				
				System.err.println("Il n'y a aucun resultat.");
				
			}
			
			monRS.next();

			// Remplir la chambre avec le ResultSet
			laChambre.setNumero(monRS.getInt(1));
			laChambre.setNumeroCategorie(monRS.getInt(2));
			laChambre.setNum_hotel(monRS.getInt(3));
			//Message d'erreur � mettre en place
		
			
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return laChambre;
	}

	//---------------------------	
	//METHODE FIND ALL
	//---------------------------

	public CollectionMetier findAll()  {

		CollectionMetier maListeDeChambres = new CollectionMetier();

		String req = "SELECT * FROM CHAMBRE";

		Statement monPS;

		try {
			monPS = ConnexionBDD.getCxion().createStatement();

			ResultSet monRS = monPS.executeQuery(req);

			if (! monRS.next()) {
				System.err.println("Pas de r�sultats !");
			}

			while(monRS.next()) {

				Chambre maChambre = new Chambre();

				// Remplir ma chambre avec le ResultSet
				maChambre.setNumero(monRS.getInt(1));
				maChambre.setNumeroCategorie(monRS.getInt(2));
				maChambre.setNum_hotel(monRS.getInt(3));

				maListeDeChambres.add(maChambre);

				System.out.println("Chambre n� " + monRS.getInt(1) + "\n" +
						"Categorie : " + monRS.getInt(2) + 	"\n" +
						"Num�ro Hotel : " + monRS.getInt(3));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (UOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return maListeDeChambres;
	}


	// ---------------------------	
	//METHODE FIND BY CRITERIA
	// ---------------------------
	@Override
	public CollectionMetier findByCriteria (Chambre ChambreAtrouver) {

		CollectionMetier Liste_Resultat = new CollectionMetier();
		String sql = "SELECT * FROM CHAMBRE WHERE ";
		Boolean parametre_existant = false;
		Integer compteur_chambre = 0;

		if (ChambreAtrouver.getNumero() != 0 && parametre_existant == false) {

			sql += "NUCHAMBRE = " + ChambreAtrouver.getNumero();
			parametre_existant = true;
		}

		if (ChambreAtrouver.getNumeroCategorie() != 0 && parametre_existant == false ) {

			sql += "NUCATCHAMBRE = " + ChambreAtrouver.getNumero();
			parametre_existant = true;

		} else if (ChambreAtrouver.getNumeroCategorie() != 0 && parametre_existant == true ) {
			sql += " AND NUCATCHAMBRE = " + ChambreAtrouver.getNumero();
			parametre_existant = true;
		}

		if (ChambreAtrouver.getNum_hotel() != 0 && parametre_existant == false ) {
			sql += "NUHOTEL = " + ChambreAtrouver.getNum_hotel();
			parametre_existant = true;

		} else if (ChambreAtrouver.getNum_hotel() != 0 && parametre_existant == true ) {

			sql += " AND NUHOTEL = " + ChambreAtrouver.getNum_hotel();
			parametre_existant = true;
		}

		try {
			PreparedStatement resultat = ConnexionBDD.getCxion().prepareStatement(sql);
			ResultSet Selection = resultat.executeQuery();	
			System.out.print(sql + "\n");

			if (!Selection.next()) {
				System.err.println("Chambre introuvable.");

			}
			while(Selection.next()){
				Chambre maChambre = new Chambre();
				//Remplir ma chambre avec le ResultSet
				maChambre.setNumero(Selection.getInt(1));
				maChambre.setNumeroCategorie(Selection.getInt(2));
				maChambre.setNum_hotel(Selection.getInt(3));
				Liste_Resultat.add(maChambre);
				compteur_chambre ++;


				System.out.println	(	"Compteur Chambre " + compteur_chambre + 	"\n" +
						"Chambre n� " + maChambre.getNumero() + 	"\n" +
						"Categorie : " + maChambre.getNumeroCategorie() + 	"\n" +
						"Num�ro Hotel : " + maChambre.getNum_hotel()
						);


			}
			//Message d'erreur � mettre en place
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return Liste_Resultat;
	}
	
	@Override
	public Chambre findById(int idChambreARechercher) {
		return null;
		}
}













//Remplacer ArrayList par CollectionMetiers
/*
	public CollectionMetiers findAll() {

		CollectionMetiers maListeDeChambres = new CollectionMetiers();

		String req = "SELECT * FROM CHAMBRE";

		Statement monPS;

		try {
			monPS = ConnexionBDD.getCxion().createStatement();

			ResultSet monRS = monPS.executeQuery(req);

			while(monRS.next()) {

				Chambre maChambre = new Chambre();

				// Remplir ma chambre avec le ResultSet
				maChambre.setNumero(monRS.getInt(1));
				maChambre.setNumeroCategorie(monRS.getInt(2));
				maChambre.setNum_hotel(monRS.getInt(3));

				maListeDeChambres.add(maChambre);

				System.out.println("Chambre n� " + monRS.getInt(1) + "\n" +
						"Categorie : " + monRS.getInt(2) + 	"\n" +
						"Num�ro Hotel : " + monRS.getInt(3));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return maListeDeChambres;
	}
 */
/*
	@Override
			//Message d'erreur � mettre en place
public CollectionMetiers findbyCriteria (Chambre ChambreAtrouver) {

		CollectionMetiers Liste_Resultat = new CollectionMetiers();
		String sql = "SELECT * FROM CHAMBRE WHERE ";
		Boolean parametre_existant = false;
		Integer compteur_chambre = 0;

		if (ChambreAtrouver.getNumero() != 0 && parametre_existant == false) {

			sql += "NUCHAMBRE = " + ChambreAtrouver.getNumero();
			parametre_existant = true;
		}

		if (ChambreAtrouver.getNumeroCategorie() != 0 && parametre_existant == false ) {

			sql += "NUCATCHAMBRE = " + ChambreAtrouver.getNumero();
			parametre_existant = true;

		} else if (ChambreAtrouver.getNumeroCategorie() != 0 && parametre_existant == true ) {
			sql += " AND NUCATCHAMBRE = " + ChambreAtrouver.getNumero();
			parametre_existant = true;
	}

	if (ChambreAtrouver.getNum_hotel() != 0 && parametre_existant == false ) {
		sql += "NUHOTEL = " + ChambreAtrouver.getNum_hotel();
		parametre_existant = true;

	} else if (ChambreAtrouver.getNum_hotel() != 0 && parametre_existant == true ) {

		sql += " AND NUHOTEL = " + ChambreAtrouver.getNum_hotel();
		parametre_existant = true;
	}

		try {
			PreparedStatement resultat = ConnexionBDD.getCxion().prepareStatement(sql);
			ResultSet Selection = resultat.executeQuery();	
			System.out.print(sql + "\n");

			if (!Selection.next()) {
				System.err.println("Chambre introuvable.");

			}
			while(Selection.next()){
				Chambre maChambre = new Chambre();
				//Remplir ma chambre avec le ResultSet
				maChambre.setNumero(Selection.getInt(1));
				maChambre.setNumeroCategorie(Selection.getInt(2));
				maChambre.setNum_hotel(Selection.getInt(3));
				Liste_Resultat.add(maChambre);
				compteur_chambre ++;


				System.out.println	(	"Compteur Chambre " + compteur_chambre + 	"\n" +
										"Chambre n� " + maChambre.getNumero() + 	"\n" +
										"Categorie : " + maChambre.getNumeroCategorie() + 	"\n" +
										"Num�ro Hotel : " + maChambre.getNum_hotel()
									);


			}
					//Message d'erreur � mettre en place
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return Liste_Resultat;
	}

	public ArrayList<Chambre> findAll1() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Chambre findByCriteria(Chambre ObjetARecuperer) {
		// TODO Auto-generated method stub
		return null;
	}

	}
 */
