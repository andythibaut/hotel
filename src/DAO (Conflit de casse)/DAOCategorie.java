package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import model.ConnexionBDD;
import collectionmetier.CollectionMetier;
import exceptions.UOException;
import factories.FactoryFactory;
import model.Categorie;



public class DAOCategorie implements IDAO<Categorie>{

	@Override
	public Categorie create(Categorie objACreer) {

		String req = "INSERT INTO CATEGORIE (NUCAT, NOMCAT) VALUES (?,?)";

		try {
					//connexion au server de la bdd
			PreparedStatement monPS = ConnexionBDD.getCon().prepareStatement(req);

					//recupere les valeurs de notre objets pour les metres dans notre requete
			monPS.setInt	(1,  objACreer.getNuCat());
			monPS.setString	(2,  objACreer.getNomCat());


			monPS.executeUpdate();

		} catch (SQLException e) {
			
			e.printStackTrace();
		}

		return objACreer; 


	}

	@Override
	public Categorie update(Categorie objAModifier) {

		String req = "UPDATE Categorie SET NOMCAT = ? WHERE NUCAT = ? ";



		try {
					//connexion au server de la bdd
			PreparedStatement monPS = ConnexionBDD.getCon().prepareStatement(req);

					//recupere les valeurs de notre objets pour les metres dans notre requete
			monPS.setInt	(2,  objAModifier.getNuCat());
			monPS.setString	(1,  objAModifier.getNomCat());


			monPS.executeUpdate();


		} catch (SQLException e) {
			
			e.printStackTrace();
		}


		return objAModifier;

	}

	@Override
	public void delete (Categorie objAsupprimer){

		String req="DELETE FROM CATEGORIE WHERE NOMCAT=?";
		
		try {
					//connexion au server de la bdd
		PreparedStatement monPs = ConnexionBDD.getCon().prepareStatement(req);
		
					//recupere les valeurs de notre objets pour les metres dans notre requete
		monPs.setInt	(1,  objAsupprimer.getNuCat());
		monPs.setString	(2,  objAsupprimer.getNomCat());

		monPs.executeUpdate();
		
		} catch (SQLException e) {
			
			e.printStackTrace();
		}

	}
	
	@Override
	public Categorie findById(int idARechercher) {
		
		
			Categorie maCategorie=(Categorie) FactoryFactory.getFactory(3).getTable(0);

			try {

				String req="SELECT * FROM CATEGORIE WHERE NUCAT=? ";

				PreparedStatement monPs=ConnexionBDD.getCon().prepareStatement(req);
				
					//j'affecte le premier point d'interrogation au 1 parametre de la m�thode
				monPs.setInt(1,idARechercher);

			    	//j'instancie le resultset monRS du resultat du select 
				ResultSet monRS = monPs.executeQuery();
				monRS.next();

				    //il faut mettre le numero de categorie du resultat de la recherche
				maCategorie.setNuCat	(monRS.getInt(1));
				maCategorie.setNomCat	(monRS.getString(2));

			} catch (SQLException e) {
				
				e.printStackTrace();
			}


			return maCategorie;

	}

	@Override
	public CollectionMetier findAll() {

		CollectionMetier maCollection = FactoryFactory.getFactory(0).getCollection();
		
		try {
			String req="SELECT * FROM CATEGORIE";
						//preparer la connexion
			Statement monSt=ConnexionBDD.getCon().prepareStatement(req);
						//metre le resultat de la requete dans un ResultSet
			ResultSet monRs=monSt.executeQuery(req);;
			
						//tant que il y a des resultat dans mon ResultSet, le rajoute dans ma collection 
			while(monRs.next()) {
				
				maCollection.add(new Categorie(monRs.getInt(1),monRs.getString(2)));
			}

		}catch (SQLException e) {
			
			e.printStackTrace();
		} catch (UOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return maCollection;
	}


	@Override
	public CollectionMetier findByCriteria(Categorie objARecher) {

		CollectionMetier maCollection = FactoryFactory.getFactory(0).getCollection();
	
		String req = "SELECT * FROM CATEGORIE WHERE NOMCAT LIKE: 1";

		try {
						//preparer la connexion
			PreparedStatement monPS = ConnexionBDD.getCon().prepareStatement(req);

						//remplacer le 1 de notre requete par un critere
			String param = "%" + objARecher.getNomCat() + "%";
			monPS.setString(1, param);

			ResultSet monRS = monPS.executeQuery();
						
						//pour chaque resultat dans mon ResultSet cr�er un objet et le rajouter dans une collection
			while (monRS.next()) {
				
				maCollection.add(new Categorie(monRS.getInt(1), monRS.getString(2)));
			}
			
	
		

		} catch (SQLException e) {
			
			e.printStackTrace();
		} catch (UOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

		return maCollection;
	}






}






