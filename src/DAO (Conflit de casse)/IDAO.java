package dao;

import collectionmetier.CollectionMetier;

public interface IDAO <T> {

	public T create(T pObjetACreer);

	public T update(T pObjetAModifier);

	public void delete(T pObjetASupprimer);

	public T findById(int pIdObjetRecherche);

	public CollectionMetier findAll();

	public CollectionMetier findByCriteria(T pObjetRecherche);
}
