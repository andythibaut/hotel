/**
 * 
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import collectionmetier.CollectionMetier;
import exceptions.UOException;
import model.Chambre;
import model.ConnexionBDD;
import model.Hotel;

/**
 * @author GROUPE1
 *
 */
public class DAOHotel implements IDAO<Hotel> {

	//  C R E  A T E
	
	//Je veux créer des hotels.
		@Override
		
/**
 *  * Je veux créer un hotel dont son numéro sera celui du dernier numero hotel et ajouter un 
 * 
* */
		
	//Perla 
	public Hotel create(Hotel hotelACreer) {
		// Récuperation du dernier numero d'hotel 
		String req1= "SELECT MAX(NUHOTEL) FROM HOTEL";
		try {
			PreparedStatement monPS1 = ConnexionBDD.getCxion().prepareStatement(req1);
			monPS1.executeQuery(req1);
			
			ResultSet monRS = monPS1.executeQuery();
			System.out.println("OK");
			monRS.next();
			int monNumHotel = monRS.getInt(1) +1;
			
			hotelACreer.setNumeroHotel(monNumHotel);
			
			
	
		String req = "INSERT INTO HOTEL (NUHOTEL, NUTYPEHOTEL, NOMHOTEL, ADRESSEHOTEL, CPHOTEL, VILLEHOTEL) VALUES (?, ?, ?, ?, ?, ?)";
		PreparedStatement monPS = ConnexionBDD.getCxion().prepareStatement(req); 
			monPS.setInt	(1,  monNumHotel); //je recupere ma req1
			monPS.setInt	(2, hotelACreer.getNbEtoile());
			monPS.setString	(3, hotelACreer.getNom());
			monPS.setString	(4, hotelACreer.getAdresse());
			monPS.setString	(5, hotelACreer.getCodePostal());
			monPS.setString	(6, hotelACreer.getVille());
			//System.out.println("OK");
			monPS.executeUpdate();
			
		//A vérifier s'il y a un trigger lors de la création	
			
		} catch (SQLException e) {
			
		
			e.printStackTrace();
		
			}
		
			
		return hotelACreer;
}	

//Andy
	@Override
	public void delete(Hotel objAsupprimer) {
		
		
		// TODO A FAIRE !!!!
		//fait appel à DAO_Chambre().delete() pour supprimer toutes les chambres de l'hotel
		// on travaille sur un arrayList provenant de la DAO_Chambre
		//CollectionMetier maListe = new DaoChambre().findByCriteria(new Chambre (0,0, objAsupprimer.getNumeroHotel()));
		// Pour chaque ligne de la liste, je supprime la chambre.
		/*for (int i=1 ; i<= maListe.size(); i++) {
			try {
			// TODO CHECKER LA METHODE POUR GET LA Collection Métier.
				new DaoChambre().delete(maListe.get(i));
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		// Une fois toutes chambres supprimées, je peux supprimer l'hotel
		
		String req = "DELETE FROM HOTEL WHERE NUHOTEL = ?";
		
		try{
			PreparedStatement monPS = ConnexionBDD.getCxion().prepareStatement(req);
			monPS.setInt(1, objAsupprimer.getNumeroHotel());
			monPS.executeUpdate();
		}
		catch (Exception e){
			e.getMessage();
		}*/
	}
		
	
//Tareq 
	@Override
	public Hotel findById(int HotelChercher) {
		
		Hotel monTP = new Hotel();
		
		String req = "SELECT * FROM HOTEL WHERE numeroHotel = ?";
				
		try {
			PreparedStatement monPS = ConnexionBDD.getCxion().prepareStatement(req);
		
			// TO DO A CHECKER 
			monPS.setInt(1, HotelChercher);	
		
			ResultSet monRS = monPS.executeQuery();
		
			monRS.next();
		
		//recupere les infos des colonnes de mon reusultset pour les set sur mon objet
		
			monTP.setNumeroHotel(monRS.getInt(1));   
			monTP.setNbEtoile(monRS.getInt(2));
			monTP.setNom(monRS.getString(3));
			monTP.setAdresse(monRS.getString(4));
			monTP.setCodePostal(monRS.getString(5));
			monTP.setVille(monRS.getString(6));
			// TO DO SELON DAO CHAMBRE A VOIR AVEC GROUPE 2
		//on attend dao chambre pour nom de class et nom de methode
			//monTP.setMesChambres(new DAO_Chambre.getFindByCriteria(HotelChercher)); 
		 
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return monTP;
	}
	

//andy
	@Override
	public CollectionMetier findAll() {
		CollectionMetier maListe = new CollectionMetier();
		//Ecriture de la requete
		String req = "SELECT * FROM HOTEL";
		
		try{
			Statement monPS = ConnexionBDD.getCxion().prepareStatement(req);
			ResultSet monRS = monPS.executeQuery(req);
			while (monRS.next()){
				maListe.add(new Hotel (monRS.getInt(1), monRS.getInt(2), monRS.getString(3), monRS.getString(4),
						monRS.getString(5), monRS.getString(6), null));
			}
		}
		catch (Exception e){
			e.getMessage();
		}
		return maListe;
	}



//Perla

@Override
	public Hotel update(Hotel hotelAModifier) {
		// Je veux modifier les hotels.
		
		
		String req = "UPDATE HOTEL "
				+ "SET NUTYPEHOTEL = ? "
				+ "WHERE NUHOTEL = ? ";
		
		try {
			PreparedStatement monPS = ConnexionBDD.getCxion().prepareStatement(req);
			
			monPS.setInt	(1, hotelAModifier.getNbEtoile());
			monPS.setInt	(2, hotelAModifier.getNumeroHotel());
			
			monPS.executeUpdate();
			
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
			
		return hotelAModifier;
	}
	
// MATHIEU
	@Override
	public CollectionMetier findByCriteria(Hotel objARechercher) {
		// TODO Auto-generated method stub
		// CREATION DE l'ARRAYLIST à retourner
		
		CollectionMetier tousLesHotelsParCritere = new CollectionMetier();
		String req = "SELECT * FROM HOTEL WHERE";
		// DAns l'objet passé en parametre si les attribut sont non null ou non = 0 
		//je les prends en compte pour construire la requete
		boolean criterePrecedentExistant = false;
		// VERIFICATION SI L'ATTRIBUT NUMERO HOTEL est renseigné
		if (objARechercher.getNumeroHotel() != 0) {
			req = req + " NUHOTEL = "+objARechercher.getNumeroHotel();
			criterePrecedentExistant = true;
		}
		// VERIFICATION SI l'ATTRIBUT NOM HOTEL est renseigné
		if (objARechercher.getNom() != null) {
			if (criterePrecedentExistant) {
				req = req + " AND NOMHOTEL = '"+objARechercher.getNom()+"'";								
			} else {
				req = req + " NOMHOTEL = '"+objARechercher.getNom()+ "'";
				criterePrecedentExistant = true;			
			}
		}
		// VERIFICATION SI L'ATTRIBUT NOMBRE ETOILE EST RENSEIGNE
		if (objARechercher.getNbEtoile() != 0) {
			if (criterePrecedentExistant) {
				req = req + " AND NUTYPEHOTEL = '"+objARechercher.getNbEtoile()+"'";
			} else {
				req = req + " NUTYPEHOTEL = '"+objARechercher.getNbEtoile()+ "'";
				criterePrecedentExistant = true;
			}
		}
		
		// VERIFICATION SI L'ATTRIBUT ADRESSE EST RENSEIGNE
		if (objARechercher.getAdresse() != null) {
			if (criterePrecedentExistant) {
				req = req + " AND ADRESSEHOTEL = '"+objARechercher.getAdresse()+"'";
			} else {
				req = req + " ADRESSEHOTEL = '"+objARechercher.getAdresse()+ "'";
				criterePrecedentExistant = true;
			}
		}
		// VERIFICATION SI L'ATTRIBUT CODE POSTAL EST RENSEIGNE
		if (objARechercher.getCodePostal() != null) {
			if (criterePrecedentExistant) {
				req = req + " AND CPHOTEL = '"+objARechercher.getCodePostal()+"'";
			} else {
				req = req + " CPHOTEL = '"+objARechercher.getCodePostal()+ "'";
				criterePrecedentExistant = true;
			}
		}
		
		// VERIFICATION SI L'ATTRIBUT LA VILLE EST RENSEIGNE
		if (objARechercher.getVille() != null) {
			if (criterePrecedentExistant) {
				req = req + " AND VILLEHOTEL = '"+objARechercher.getVille()+"'";
			} else {
				req = req + " VILLEHOTEL = '"+objARechercher.getVille()+ "'";
				criterePrecedentExistant = true;
			}
		}	
				
		
		try {
			PreparedStatement ps = ConnexionBDD.getCxion().prepareStatement(req);
			ResultSet rs = ps.executeQuery();
			//tant qu'il y a des lignes dans resultset
			while (rs.next()) { // Creation d'un objet hotel
				Hotel hotelEncours = new Hotel();
				// MAJ DES ATTRIBUTS DE L'OBJET
				hotelEncours.setNumeroHotel(rs.getInt(1));
				hotelEncours.setNom(rs.getString(3));
				hotelEncours.setNbEtoile(rs.getInt(2));
				hotelEncours.setAdresse(rs.getString(4));
				hotelEncours.setCodePostal(rs.getString(5));
				hotelEncours.setVille(rs.getString(6));
				//hotelEncours.setActif(rs.getInt(7));
				//System.out.println();
				
				// Ajout de cette Hotel à la ArrayList
				tousLesHotelsParCritere.add(hotelEncours);	
			}	
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (UOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return tousLesHotelsParCritere;
	}


	
}