package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.ConnexionBDD;
import collectionmetier.CollectionMetier;
import exceptions.UOException;
import factories.FactoryFactory;
import model.TypeHotel;
//
public class DAOTypeHotel implements IDAO<TypeHotel> {

	@Override
	
	public TypeHotel create(TypeHotel objACreer) {
		
		String req = "INSERT INTO TYPE_HOTEL(NUTYPE,NOMTYPE) VALUES (?,?)";

		try {
			// recupere connexion
			PreparedStatement monPS = ConnexionBDD.getCon().prepareStatement(req);
			
			//on affecte  la variable monPS des resultats de la requete
			monPS.setInt(1,objACreer.getNuType());
			monPS.setString(2, objACreer.getNomType());
			
			monPS.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return objACreer;
	}

	@Override
	public TypeHotel update(TypeHotel objAModifier) {
		// TODO Auto-generated method stub
		String req = "UPDATE TYPE_HOTEL SET NOMTYPE = ? WHERE NUTYPE = ?";

		try {
			PreparedStatement monPS = ConnexionBDD.getCon().prepareStatement(req);
			
			monPS.setString(1, objAModifier.getNomType());
			monPS.setInt(2,objAModifier.getNuType());
			
			monPS.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return objAModifier;
	}

	@Override
	public void delete(TypeHotel objAsupprimer){
		// TODO Auto-generated method stub
		String req = "DELETE FROM TYPE_HOTEL WHERE NUTYPE = ?";
		
		try {
			PreparedStatement monPS = ConnexionBDD.getCon().prepareStatement(req);
	
			monPS.setInt (1, objAsupprimer.getNuType());

		
			monPS.executeUpdate();
			
		} catch (SQLException e) {
	
			e.printStackTrace();
		}
	}

	@Override
	public TypeHotel findById(int idARechercher) {

			TypeHotel laEstrella = (TypeHotel) FactoryFactory.getFactory(3).getTable(3);

			String req = "SELECT * FROM TYPE_HOTEL WHERE NUTYPE = ?";

			try {

				PreparedStatement monPS = ConnexionBDD.getCon().prepareStatement(req);
				monPS.setInt(1, idARechercher);			
				ResultSet monRS = monPS.executeQuery();
				monRS.next();
				// ici je veux remplir mon objet chambre avec les valeurs recup en BDD
				laEstrella.setNuType(monRS.getInt(1));
				laEstrella.setNomType(monRS.getString(2));

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return laEstrella;

	}

	@Override
	public CollectionMetier findAll() {

		CollectionMetier maListeTypeHotel = FactoryFactory.getFactory(0).getCollection();

		String req = "SELECT * FROM TYPE_HOTEL";
		try {

			PreparedStatement monPS = ConnexionBDD.getCon().prepareStatement(req);	
			ResultSet monRS = monPS.executeQuery();

			while (monRS.next()) {

				maListeTypeHotel.add(new TypeHotel(monRS.getInt(1), monRS.getString(2)));
			}


		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.getMessage();
		} catch (UOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		return maListeTypeHotel;
	}

	@Override
	public CollectionMetier findByCriteria(TypeHotel objARechercher) {


		CollectionMetier maListeTypeHotel = FactoryFactory.getFactory(0).getCollection();

		//String req = "SELECT * FROM TYPE_HOTEL WHERE NUTYPE LIKE :1 AND NOMTYPE LIKE: 2";
		String req = "SELECT * FROM TYPE_HOTEL WHERE NOMTYPE LIKE :1";


		try {
			PreparedStatement monPS = ConnexionBDD.getCon().prepareStatement(req);

			// monPS.setInt(1, objARechercher.getNutype());
			// monPS.setString(2, "3%");
			String param ="%" + objARechercher.getNomType() + "%";
			monPS.setString(1, param);

			ResultSet monRS = monPS.executeQuery();

			while (monRS.next()) {

				maListeTypeHotel.add(new TypeHotel(monRS.getInt(1), monRS.getString(2)));

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

		return maListeTypeHotel;
	}

}
