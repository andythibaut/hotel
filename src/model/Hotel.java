package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import collectionmetier.CollectionMetier;
import collectionmetier.Collectionnable;
import exceptions.UOException;



public class Hotel implements Collectionnable, Table {
	//Attributs 
	private int numeroHotel;
	private int nbEtoile;
	private String nom;
	private String adresse;
	private String codePostal;
	private String ville;
	private CollectionMetier mesChambres = new CollectionMetier();
	//
	/**
	 * @param numeroHotel
	 * @param nbEtoile
	 * @param nom
	 * @param adresse
	 * @param codePostal
	 * @param ville
	 * @param mesChambres
	 */
	public Hotel(int numeroHotel, int nbEtoile, String nom, String adresse, String codePostal, String ville,
			CollectionMetier mesChambres) {
		super();
		this.numeroHotel = numeroHotel;
		this.nbEtoile = nbEtoile;
		this.nom = nom;
		this.adresse = adresse;
		this.codePostal = codePostal;
		this.ville = ville;
		this.mesChambres = mesChambres;
	}
	/**
	 * 
	 */
	public Hotel() {
		super();
		// TODO Auto-generated constructor stub
	}
	public int getNumeroHotel() {
		return numeroHotel;
	}
	public void setNumeroHotel(int numeroHotel) {
		this.numeroHotel = numeroHotel;
	}
	public int getNbEtoile() {
		return nbEtoile;
	}
	public void setNbEtoile(int nbEtoile) {
		this.nbEtoile = nbEtoile;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getAdresse() {
		return adresse;
	}
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	public String getCodePostal() {
		return codePostal;
	}
	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}
	public String getVille() {
		return ville;
	}
	public void setVille(String ville) {
		this.ville = ville;
	}
	public CollectionMetier getMesChambres() {
		return mesChambres;
	}
	public void setMesChambres(CollectionMetier mesChambres) {
		this.mesChambres = mesChambres;
	}
	
	public void addChambre(Chambre pChambre) {
//		mesChambres.add(pChambre.getNumero(), pChambre);
		try {
			mesChambres.add(pChambre);
		} catch (UOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public long aficcherNbChambre() {
		//long i = 0;
		return this.mesChambres.size();
	}

	@Override
	public String toTestString() {
		// TODO Auto-generated method stub
		return null;
	}
}
