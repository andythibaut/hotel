package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnexionBDD {

	public static final String DRV = "oracle.jdbc.driver.OracleDriver";
	public static final String URL = "jdbc:oracle:thin:@localhost:1521:xe";

	private static String usr;
	private static String pwd;

	private static Connection con;

	/**
	 * @param usr the usr to set
	 */
	public static void setUsr(String pUsr) {
		usr = pUsr;
	}

	/**
	 * @param pwd the pwd to set
	 */
	public static void setPwd(String pPwd) {
		pwd = pPwd;
	}

	/**
	 * @return the con
	 */
	public static Connection getCon() {
		if (con == null) initBDD(usr, pwd);
		return con;
	}
	
	public static Connection getCxion() {
		if (con == null) initBDD(usr, pwd);
		return con;
	}

	public static void initBDD(String pUsr, String pPwd) {

		try {

			//chargement de la classe du driver
			Class.forName(DRV);
			System.out.println("chargement du driver reussi");

			//connexion a la database avec url, usr et pwd
			con = DriverManager.getConnection(URL, pUsr, pPwd);
			System.out.println("connexion etablie");

			con.setAutoCommit(false);

		} catch(ClassNotFoundException e) {

			e.printStackTrace();
			System.out.println("echec de chargement du driver");

		} catch(SQLException e) {

			e.printStackTrace();
			System.out.println("echec de connexion");
		}

		usr = pUsr;
		pwd = pPwd;
	}
}
