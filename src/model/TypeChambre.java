/**
 * 
 */
package model;

/**
 * @author 31010-07-13
 *
 */
public class TypeChambre {

	private int 	nucatChambre;
	private String 	nomType;
	
	
	public TypeChambre() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public TypeChambre(int nucatChambre, String nomType) {
		super();
		this.nucatChambre = nucatChambre;
		this.nomType = nomType;
	}
	
	public int getNucatChambre() {
		return nucatChambre;
	}
	
	public void setNucatChambre(int nucatChambre) {
		this.nucatChambre = nucatChambre;
	}
	
	public String getNomType() {
		return nomType;
	}
	
	public void setNomType(String nomType) {
		this.nomType = nomType;
	}
	
	
}
