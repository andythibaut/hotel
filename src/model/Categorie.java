/**
 * 
 */
package model;

import collectionmetier.Collectionnable;

/**
 * @author 31010-07-03
 *
 */
public class Categorie implements Collectionnable, Table {
	
	private int NuCat;
	private String NomCat;
	
	// ++++++++   Constructors
	public Categorie (int nuCat, String nomCat) {
		super();
		NuCat = nuCat;
		NomCat = nomCat;
	} 
	
	public Categorie() {
		super();

	}

	// ++++++++++    Getters and Setters
	public int getNuCat() {
		return NuCat;
	}


	public void setNuCat(int nuCat) {
		NuCat = nuCat;
	}


	public String getNomCat() {
		return NomCat;
	}


	public void setNomCat(String nomCat) {
		NomCat = nomCat;
	}

	@Override
	public String toTestString() {
		// TODO Auto-generated method stub
		return null;
	} 
	
}
