package model;

import collectionmetier.Collectionnable;

public class Chambre implements Collectionnable, Table {
	//Attributs
	private int numero;
	private int numeroCategorie;
	private int num_hotel;
	/**
	 * @param numero
	 * @param numeroCategorie
	 */
	public Chambre(int numero, int numeroCategorie, int num_hotel) {
		super();
		this.numero = numero;
		this.numeroCategorie = numeroCategorie;
		this.num_hotel = num_hotel;
	}
	/**
	 * @return the num_hotel
	 */
	public int getNum_hotel() {
		return num_hotel;
	}
	/**
	 * @param num_hotel the num_hotel to set
	 */
	public void setNum_hotel(int num_hotel) {
		this.num_hotel = num_hotel;
	}

	public Chambre() {
		super();
		// TODO Auto-generated constructor stub
	}
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public int getNumeroCategorie() {
		return numeroCategorie;
	}
	public void setNumeroCategorie(int numeroCategorie) {
		this.numeroCategorie = numeroCategorie;
	}
	@Override
	public String toTestString() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}
