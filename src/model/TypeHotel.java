/**
 * 
 */
package model;

import collectionmetier.Collectionnable;

/**
 * @author 31010-07-03
 *
 */
public class TypeHotel implements Collectionnable, Table {
	private int Nutype;
	private String NomType; 
	
	//++++++     Constructors
	
	public TypeHotel(int nutype, String nomType) {
		super();
		Nutype = nutype;
		NomType = nomType;
	}
	
	public TypeHotel() {
		super();
	}
	
	//++++      Getters and Setters

	public int getNuType() {
		return Nutype;
	}

	public void setNuType(int nutype) {
		Nutype = nutype;
	}

	public String getNomType() {
		return NomType;
	}

	public void setNomType(String nomType) {
		NomType = nomType;
	}

	@Override
	public String toTestString() {
		// TODO Auto-generated method stub
		return null;
	}
	

}
