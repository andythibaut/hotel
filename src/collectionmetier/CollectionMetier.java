package collectionmetier;
import java.util.ArrayList;
import java.util.HashMap;
import exceptions.UOException;
import model.Categorie;
import model.Chambre;
import model.Hotel;
import model.TypeHotel;

public class CollectionMetier {

	private HashMap<Integer, Collectionnable> maListe = new HashMap<Integer, Collectionnable>();

	//ajouter � la collection un objet pass� en param�tre 
	public void add(Collectionnable element) throws UOException {

		switch (element.getClass().getSimpleName()) {

			case "Categorie": 
				this.maListe.put(((Categorie) element).getNuCat(), element);
				break;
			case "TypeHotel":	
				this.maListe.put(((TypeHotel) element).getNuType(), element); 
				break;
			case "Hotel": 	
				this.maListe.put(((Hotel) element).getNumeroHotel(), element);
				break;
			case "Chambre":	
				this.maListe.put(
						((Chambre) element).getNum_hotel() * 10000 +
						((Chambre) element).getNumero(), 
						element);
				break;
			default :
				throw new UOException("Objet non identifi�, appelez Mulder et Scully"); 
		}
	}

	//ajouter � la collection une autre pass�e en param�tre
	public void addAll(CollectionMetier c) {

		this.maListe.putAll(c.maListe);
	}

	//supprimer un objet de la collection
	public void remove(Collectionnable element) throws UOException {

			switch (element.getClass().getSimpleName()) {

			case "Categorie": 
				this.maListe.remove(((Categorie) element).getNuCat(), element);
			case "TypeHotel":	
				this.maListe.remove(((TypeHotel) element).getNuType(), element); 
			case "Hotel": 	
				this.maListe.remove(((Hotel) element).getNumeroHotel(), element);
			case "Chambre":	
				this.maListe.remove(
						((Chambre) element).getNum_hotel() * 10000 +
						((Chambre) element).getNumero(), 
						element);
			default :
				throw new UOException("Objet non identifi�, appelez Mulder et Scully"); 
		}
	}

	//retourner la taille de la collection
	public int size() {

		return this.maListe.size();
	}

	//vider la collection
	public void clear(){

		this.maListe.clear();
	}

	//retourner si oui ou non la collection contient l'objet pass� en param�tre
	public boolean contains(Collectionnable element) throws UOException {

			switch (element.getClass().getSimpleName()) {

			case "Categorie":
				return this.maListe.containsKey(((Categorie) element).getNuCat());
			case "TypeHotel":	
				return this.maListe.containsKey(((TypeHotel) element).getNuType()); 
			case "Hotel": 	
				return this.maListe.containsKey(((Hotel) element).getNumeroHotel());
			case "Chambre":	
				return this.maListe.containsKey(
						((Chambre) element).getNum_hotel() * 10000 +
						((Chambre) element).getNumero());
			default :
				throw new UOException("Objet non identifi�, appelez Mulder et Scully");
		}
	}
	
	//Retourner la collection métier en arrayList
	public ArrayList<Collectionnable> convertir () {
        
        ArrayList<Collectionnable> MaListeDeResultat = new ArrayList<Collectionnable>();
        
        MaListeDeResultat.addAll(maListe.values());
        
        return MaListeDeResultat;        
    }
}
