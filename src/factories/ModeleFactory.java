package factories;

import collectionmetier.CollectionMetier;
import dao.IDAO;
import model.Table;

public abstract class ModeleFactory {

	public abstract IDAO getDAO(int pType);

	public abstract Table getTable(int pType);

	public abstract CollectionMetier getCollection();
}
