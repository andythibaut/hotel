package factories;

import collectionmetier.CollectionMetier;
import dao.DAOCategorie;
import dao.DAOChambre;
import dao.DAOHotel;
import dao.DAOTypeHotel;
import dao.IDAO;
import model.Table;

public class DAOFactory extends ModeleFactory {

	public static final int DAOCATEGORIE	= 0;
	public static final int DAOCHAMBRE		= 1;
	public static final int DAOHOTEL		= 2;
	public static final int DAOTYPEHOTEL	= 3;

	@Override
	public IDAO getDAO(int pType) {

		switch (pType) {

			case DAOCATEGORIE :
				return new DAOCategorie();
			case DAOCHAMBRE :
				return new DAOChambre();
			case DAOHOTEL :
				return new DAOHotel();
			case DAOTYPEHOTEL :
				return new DAOTypeHotel();
			default : 
				return null;
			}
	}

	@Override
	public Table getTable(int pType) {
		return null;
	}

	@Override
	public CollectionMetier getCollection() {
		return null;
	}
}
