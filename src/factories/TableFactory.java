package factories;

import collectionmetier.CollectionMetier;
import dao.IDAO;
import model.Categorie;
import model.Chambre;
import model.Hotel;
import model.Table;
import model.TypeHotel;

public class TableFactory extends ModeleFactory {

	public static final int CATEGORIE	= 0;
	public static final int CHAMBRE		= 1;
	public static final int HOTEL		= 2;
	public static final int TYPEHOTEL	= 3;

	@Override
	public IDAO getDAO(int pType) {
		return null;
	}

	@Override
	public Table getTable(int pType) {

		switch (pType) {

		case CATEGORIE :
			return new Categorie();
		case CHAMBRE :
			return new Chambre();
		case HOTEL :
			return new Hotel();
		case TYPEHOTEL :
			return new TypeHotel();
		default : 
			return null;
		}
	}

	@Override
	public CollectionMetier getCollection() {
		return null;
	}
}
