/**
 * 
 */
package DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import model.ConnexionBDD;
import model.TypeHotel;

/**
 * @author andy
 *
 */
public class DAO_TypeHotel implements iDAO<TypeHotel> {

	/**
	 * 
	 */
	public DAO_TypeHotel() {
		// TODO Auto-generated constructor stub
	}

	public TypeHotel create(TypeHotel hotelACreer) {
		String req = "INSERT INTO TYPE_HOTEL VALUES (?,?)";
		
		try {
			PreparedStatement monPS = ConnexionBDD.getCxion().prepareStatement(req);
			monPS.setInt(1, hotelACreer.getNuType());
			monPS.setString(2, hotelACreer.getNomType());
			
			//execution du preparestatement
			monPS.executeUpdate();
		}
		
		catch(Exception e) {
			e.getMessage();
		}
		return hotelACreer;
	}

	public TypeHotel update(TypeHotel typeHotelAModifier) {
		String req = " UPDATE CATEGORIE SET NOMTYPE = ? WHERE NUTYPE = ?";
		
		try {
			PreparedStatement monPS = ConnexionBDD.getCxion().prepareStatement(req);
			// Je renseigne les valeurs de la chambre
			monPS.setString(1, typeHotelAModifier.getNomType());
			monPS.setInt(2, typeHotelAModifier.getNuType());
			
			monPS.executeUpdate();		
		}	
		catch (Exception e) {
			e.getMessage();
		}
		
		return typeHotelAModifier;
	}

	public void delete(TypeHotel typeASupprimer) {
		String req = "DELETE FROM TYPE_HOTEL WHERE NUTYPE = ?";
		
		try{
			PreparedStatement monPS = ConnexionBDD.getCxion().prepareStatement(req);
			monPS.setInt(1, typeASupprimer.getNuType());
			monPS.executeUpdate();
		}
		catch (Exception e){
			e.getMessage();
		}
	}

	@Override
	public TypeHotel findById(int objATrouver) {
		
		TypeHotel typeATrouver = new TypeHotel();
		String req = "SELECT * FROM CATEGORIE WHERE NUTYPE = ?";
		
		try{
			Statement monPS = ConnexionBDD.getCxion().prepareStatement(req);
			ResultSet monRS = monPS.executeQuery(req);
			monRS.next();
			typeATrouver = new TypeHotel(monRS.getInt(1), monRS.getString(2));
		}
		catch (Exception e){
			e.getMessage();
		}
		return typeATrouver;
	}

	@Override
	public ArrayList<TypeHotel> findAll() {
		ArrayList<TypeHotel> maListe = new ArrayList<TypeHotel>();
		//Ecriture de la requete
		String req = "SELECT * FROM TYPE_HOTEL";
		try{
			Statement monPS = ConnexionBDD.getCxion().prepareStatement(req);
			ResultSet monRS = monPS.executeQuery(req);
			while (monRS.next()){
				maListe.add(new TypeHotel (monRS.getInt(1), monRS.getString(2)));
			}
		}
		catch (Exception e){
			e.getMessage();
		}
		return maListe;
	}

	@Override
	public TypeHotel findByCriteria() {
		// TODO Auto-generated method stub
		return null;
	}


}
