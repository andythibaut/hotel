/**
 * 
 */
package DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import model.Chambre;
import model.ConnexionBDD;
import model.Hotel;

/**
 * @author 31010-07-13
 *
 */
public class DAO_Hotel implements iDAO<Hotel>{


	public DAO_Hotel() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public Hotel create(Hotel nouvHotel) {
		String req = "INSERT INTO HOTEL VALUES (?,?,?,?,?,?)";
		
	//	int numeroHotel, int nbEtoile, String nom, String adresse, String codePostal, String ville,
		
		try {
			PreparedStatement monPS = ConnexionBDD.getCxion().prepareStatement(req);
			monPS.setInt(1, nouvHotel.getNumeroHotel());
			monPS.setInt(2, nouvHotel.getNbEtoile());
			monPS.setString(3, nouvHotel.getNom());
			monPS.setString(4, nouvHotel.getAdresse());
			monPS.setString(5, nouvHotel.getCodePostal());
			monPS.setString(6, nouvHotel.getVille());
			
			//execution du preparestatement
			monPS.executeUpdate();
		}
		
		catch(Exception e) {
			e.getMessage();
		}
	
		return nouvHotel;
	}

	@Override
	public Hotel update(Hotel hotelAModifier) {
		
		String req = "UPDATE CHAMBRE SET NUTYPEHOTEL = ? WHERE NUHOTEL = ?";
		
		try{
			PreparedStatement monPS = ConnexionBDD.getCxion().prepareStatement(req);
			monPS.setInt(1, hotelAModifier.getNbEtoile());
			monPS.setInt(2, hotelAModifier.getNumeroHotel());
			
			monPS.executeUpdate();
		}
		catch (Exception e){
			e.getMessage();
		}
		
		return hotelAModifier;
	}
	
	

	@Override
	public void delete(Hotel hotelASupprimer){
		String req = "DELETE FROM HOTEL WHERE NUHOTEL = ?";
			
		try{
			PreparedStatement monPS = ConnexionBDD.getCxion().prepareStatement(req);
			monPS.setInt(1, hotelASupprimer.getNumeroHotel());
			monPS.executeUpdate();
		}
		catch (Exception e){
			e.getMessage();
		}
		
	}

	@Override
	public Hotel findById(int objATrouver) {
		
		Hotel hotelATrouver = new Hotel();
		String req = "SELECT * FROM HOTEL WHERE NUHOTEL = ?";
		
		try{
			Statement monPS = ConnexionBDD.getCxion().prepareStatement(req);
			ResultSet monRS = monPS.executeQuery(req);
			monRS.next();
			hotelATrouver = new Hotel(monRS.getInt(1), monRS.getInt(2), monRS.getString(3),
							monRS.getString(4),	monRS.getString(5), monRS.getString(6), null );
		}
		catch (Exception e){
			e.getMessage();
		}
		return hotelATrouver;
	}

	@Override
	public ArrayList<Hotel> findAll() {
		ArrayList<Hotel> maListe = new ArrayList<Hotel>();
		//Ecriture de la requete
		String req = "SELECT * FROM HOTEL";
		try{
			Statement monPS = ConnexionBDD.getCxion().prepareStatement(req);
			ResultSet monRS = monPS.executeQuery(req);
			while (monRS.next()){
				maListe.add(new Hotel (monRS.getInt(1), monRS.getInt(2), monRS.getString(3), monRS.getString(4),
										monRS.getString(5), monRS.getString(6), null));
			}
		}
		catch (Exception e){
			e.getMessage();
		}
		return maListe;
	}

	@Override
	public Hotel findByCriteria() {
		// TODO Auto-generated method stub
		return null;
	}
}
