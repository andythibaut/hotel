package DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import model.Chambre;
import model.ConnexionBDD;

public class DAO_Chambre implements iDAO<Chambre>{

	public Chambre create(Chambre nouvelleChambre) {
		
		String req = "INSERT INTO CHAMBRE VALUES (?,?,?)";
		
		try {
			PreparedStatement monPS = ConnexionBDD.getCxion().prepareStatement(req);
			monPS.setInt(1, nouvelleChambre.getNumeroCategorie());
			monPS.setInt(2, nouvelleChambre.getNumero());
			monPS.setInt(3, nouvelleChambre.getNum_hotel()); //numero d'hotel
			
			//execution du preparestatement
			monPS.executeUpdate();
		}
		
		catch(Exception e) {
			e.getMessage();
		}
	
		return nouvelleChambre;
	}
	
	public static Chambre findById(Chambre chambreATrouver) {
		Chambre laChambre = new Chambre();
		String req =" SELECT * FROM CHAMBRE WHERE NUCHAMBRE = ? AND NUHOTEL = ?";
		
		try {
		
			PreparedStatement monPS = ConnexionBDD.getCxion().prepareStatement(req);
			monPS.setInt(1, chambreATrouver.getNumero());
			monPS.setInt(2, chambreATrouver.getNum_hotel());
			
			ResultSet monRS = monPS.executeQuery();
			monRS.next();
			// Ici, je veux remplir mon objet chambre avec les valeurs de la BDD
			laChambre.setNumero(monRS.getInt(1));
			laChambre.setNumeroCategorie(monRS.getInt(2));
			laChambre.setNum_hotel(monRS.getInt(3));		
		}
		
		catch(Exception e) {
			e.getMessage();
		}
		
		return laChambre;
	}
	
	public Chambre update(Chambre chambreAModifier) {
		
		//ecriture de l'update
		String req = " UPDATE CHAMBRE SET NUCATCHAMBRE = ? WHERE NUHOTEL = ? AND NUCHAMBRE =?";
		try {
			PreparedStatement monPS = ConnexionBDD.getCxion().prepareStatement(req);
			// Je renseigne les valeurs de la chambre
			monPS.setInt(1, chambreAModifier.getNumeroCategorie());
			monPS.setInt(3, chambreAModifier.getNumero());
			monPS.setInt(2, chambreAModifier.getNum_hotel());
			
		
			monPS.executeUpdate();
			
		}
		catch (Exception e) {
			e.getMessage();
		}
		
		return chambreAModifier;
	}
	
	public void supprimer(Chambre chambreASupprimer){
		
		//Ecriture de la suppression
		String req = " DELETE FROM CHAMBRE WHERE NUCHAMBRE = ? AND NUHOTEL = ?";
		try{
			PreparedStatement monPS = ConnexionBDD.getCxion().prepareStatement(req);
			monPS.setInt(1, chambreASupprimer.getNumero());
			monPS.setInt(2, chambreASupprimer.getNum_hotel());
			
			monPS.executeUpdate();
		}
		catch (Exception e) {
			e.getMessage();
		}
	}
	
	public Chambre findByCriteria(Chambre chambreARetrouver){
		return null;
	}
	
	public ArrayList<Chambre> findAll(){
		ArrayList<Chambre> maListe = new ArrayList<Chambre>();
		//Ecriture de la requete
		String req = "SELECT * FROM CHAMBRE";
		try{
			Statement monPS = ConnexionBDD.getCxion().prepareStatement(req);
			ResultSet monRS = monPS.executeQuery(req);
			while (monRS.next()){
				maListe.add(new Chambre (monRS.getInt(1), monRS.getInt(2), monRS.getInt(3)));
			}
		}
		catch (Exception e){
			e.getMessage();
		}
		return maListe;
	}


	@Override
	public void delete(Chambre objASupprimer) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Chambre findById(int objATrouver) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Chambre findByCriteria() {
		// TODO Auto-generated method stub
		return null;
	}
	
}
