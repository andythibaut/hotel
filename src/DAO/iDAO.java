package DAO;

import java.util.ArrayList;

public interface iDAO <T> {

	public T create (T objACreer);
	
	public T update (T objAModifier);
	
	public void delete (T objASupprimer);
	
	public T findById (int objATrouver);
	
	public ArrayList<T> findAll();
	
	public T findByCriteria();

}
