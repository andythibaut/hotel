/**
 * class factory qui permet l'instanciation de DAO et les met à disposition
 */
package DAO;

/**
 * @author andy
 *
 */
public class FactoryDAO {

	public static final int DAO_CHAMBRE 	= 1;
	public static final int DAO_HOTEL		= 2;
	public static final int DAO_TYPEHOTEL 	= 3;
	public static final int DAO_TYPECHAMBRE = 4;
	
	
	public iDAO getDAO(int maDAO){
		
		switch (maDAO){
		
			case DAO_HOTEL :
				return new DAO_Hotel();
				//break;
			case DAO_CHAMBRE :
				return new DAO_Chambre();
				//break;
			case DAO_TYPEHOTEL :
				return new DAO_TypeHotel();
				//break;
			case DAO_TYPECHAMBRE :
				return new DAO_TypeChambre();
				//break;
			default :
				return null;
		}
	}
	
	/**
	 * 
	 */
	public FactoryDAO() {
		// TODO Auto-generated constructor stub
	}

}
