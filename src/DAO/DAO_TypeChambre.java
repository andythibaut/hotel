/**
 * 
 */
package DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import model.ConnexionBDD;
import model.TypeChambre;

/**
 * @author 31010-07-13
 *
 */
public class DAO_TypeChambre implements iDAO<TypeChambre>{

	
	public DAO_TypeChambre() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public TypeChambre create(TypeChambre typeACreer) {
		String req = "INSERT INTO TypeChambre VALUES (?,?)";
		
		try {
			PreparedStatement monPS = ConnexionBDD.getCxion().prepareStatement(req);
			monPS.setInt(1, typeACreer.getNucatChambre());
			monPS.setString(2, typeACreer.getNomType());
			
			//execution du preparestatement
			monPS.executeUpdate();
		}
		
		catch(Exception e) {
			e.getMessage();
		}
	
		return typeACreer;
	}

	@Override
	public TypeChambre update(TypeChambre objAModifier) {
		
		String req = " UPDATE CATEGORIE SET NOMCAT = ? WHERE NUCAT = ?";
		
		try {
			PreparedStatement monPS = ConnexionBDD.getCxion().prepareStatement(req);
			// Je renseigne les valeurs de la chambre
			monPS.setString(1, objAModifier.getNomType());
			monPS.setInt(2, objAModifier.getNucatChambre());
			
			monPS.executeUpdate();		
		}	
		catch (Exception e) {
			e.getMessage();
		}
		
		return objAModifier;
	}

	@Override
	public void delete(TypeChambre objASupprimer) {
		String req = "DELETE FROM CATEGORIE WHERE NUCAT = ?";
		
		try{
			PreparedStatement monPS = ConnexionBDD.getCxion().prepareStatement(req);
			monPS.setInt(1, objASupprimer.getNucatChambre());
			monPS.executeUpdate();
		}
		catch (Exception e){
			e.getMessage();
		}
	}


	@Override
	public TypeChambre findById(int objATrouver) {
		TypeChambre typeATrouver = new TypeChambre();
		String req = "SELECT * FROM CATEGORIE WHERE NUCAT = ?";
		
		try{
			Statement monPS = ConnexionBDD.getCxion().prepareStatement(req);
			ResultSet monRS = monPS.executeQuery(req);
			monRS.next();
			typeATrouver = new TypeChambre(monRS.getInt(1), monRS.getString(2));
		}
		catch (Exception e){
			e.getMessage();
		}
		return typeATrouver;
	}

	@Override
	public ArrayList<TypeChambre> findAll() {
		ArrayList<TypeChambre> maListe = new ArrayList<TypeChambre>();
		//Ecriture de la requete
		String req = "SELECT * FROM CATEGORIE";
		try{
			Statement monPS = ConnexionBDD.getCxion().prepareStatement(req);
			ResultSet monRS = monPS.executeQuery(req);
			while (monRS.next()){
				maListe.add(new TypeChambre (monRS.getInt(1), monRS.getString(2)));
			}
		}
		catch (Exception e){
			e.getMessage();
		}
		return maListe;
	}

	@Override
	public TypeChambre findByCriteria() {
		// TODO Auto-generated method stub
		return null;
	}

}
