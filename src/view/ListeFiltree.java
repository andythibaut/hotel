/**
 * 
 */
package view;

import java.util.ArrayList;
import javax.swing.Box;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import collectionmetier.Collectionnable;
import factories.FactoryFactory;
import model.Hotel;

import java.awt.Dimension;
import java.awt.GridBagLayout;

/**
 * @author andy
 * La liste filtrée regarde l'état des checkbox d'un panneau et renvoie les hôtels sélectionnés
 * 
 */
public class ListeFiltree extends JPanel{
	
	
	
	public ListeFiltree() {
		
		
		//La liste filtrée va regarder l'état des checkbox du panneau WEST
		
		//JPanel panneauGlobal = new JPanel();
		Box MaBox = Box.createVerticalBox();
		setLayout(new GridBagLayout());
		// Obtenir la liste des hotels
		ArrayList<Collectionnable> listeHotel = FactoryFactory.getFactory(1).getDAO(2).findAll().convertir();
		//Pour chaque hotel, répérer les champs nom et catégorie. Les ajouter dans un nouveau Jpanel.
		for (Collectionnable mesHotels : listeHotel) {
			//Je crée un JTextpane
			JTextArea monPanneau = new JTextArea();
			// Je récupère le nom de l'hôtel et l'ajoute
			String monTexte = new String();
			monTexte += ((Hotel)mesHotels).getNom();
			monPanneau.setText(monTexte);
			MaBox.add(monPanneau);
		}
		JScrollPane jscrlpBox = new JScrollPane(MaBox);
        jscrlpBox.setPreferredSize(new Dimension(500, 200));
        add(jscrlpBox);
        setVisible(true);
	}
	
	//Méthode qui prends en parametre une collection et la filtre
	public ListeFiltree(String monFiltre) {
		 
	}
}
