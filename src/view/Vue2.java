package view;
import java.awt.GraphicsConfiguration;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;


public class Vue2 extends JFrame implements ActionListener{

	public Vue2()  {
		
		getContentPane().setLayout(null);
		
		
		// DEFINITION BOUTONS
		JButton bouton1 = new JButton("coucou");
		bouton1.setActionCommand("ACTION1");
		JButton bouton2 = new JButton("au revoir");
		bouton2.setActionCommand("ACTION2");
		
		
		// d�finis this, cad moi... cad la frame comme listener (celui qu'il faut pr�venir en cas d'ev�nement sur le bouton 
		bouton1.addActionListener(this);
		bouton2.addActionListener(this);
		
		
		
		
		// ajout des boutons au panneau
		getContentPane().add(bouton1);
		getContentPane().add(bouton2);
		
		
		// positionnement des boutons
		bouton1.setBounds(20,20, 80, 40);
		bouton2.setBounds(20,63, 80, 40);
		
		
		// affichage fenetre
		setSize(300,300);
		setVisible(true);
	}

	
	@Override
	public void actionPerformed(ActionEvent ev) {
		
		if (ev.getActionCommand().equals("ACTION1")) {
			JOptionPane.showMessageDialog(null, ev.getActionCommand(), "coucou", JOptionPane.WARNING_MESSAGE);
		} else {
			JOptionPane.showMessageDialog(null, ev.getActionCommand(), "coucou", JOptionPane.INFORMATION_MESSAGE);
		}
		
	}


}
