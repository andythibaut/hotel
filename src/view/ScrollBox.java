/**
 * 
 */
package view;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.Box;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JScrollPane;

import collectionmetier.Collectionnable;
import model.ConnexionBDD;

/**
 * @author andy
 * Classe qui construit des scrollbox à partir de son type.
 */
public class ScrollBox extends JFrame{

	public ScrollBox(String uneTable) {
		setLayout(new GridBagLayout());

		switch (uneTable) {

			case "CATEGORIE" :

				Box MaBox = Box.createVerticalBox();
				// génération des cases à cocher
				for (Collectionnable nb_objets : liste_objets) {
					JCheckBox cb = new JCheckBox(((Categorie)nb_objets).getNomCat());
					MaBox.add(cb);
				}

				JScrollPane jscrlpBox = new JScrollPane(MaBox);
				jscrlpBox.setPreferredSize(new Dimension(140, 90));
				add(jscrlpBox);
				setVisible(true);

		}
	}
}
