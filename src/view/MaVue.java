package view;

import javax.swing.WindowConstants;
import factories.DAOFactory;
import factories.FactoryFactory;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class MaVue extends JFrame {

	public MaVue()  {
		
		/*
		 * 
		 * CONFIGURATION DE LA FENETRE
		 * 
		 */
		
		setSize(200, 300);
		setTitle("Ecran de filtre");
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		
		
		/*
		 * 
		 * PANNEAUX  & LAYOUTS
		 * 
		 */
		
		JPanel panneauGlobal = new JPanel();
		panneauGlobal.setLayout(new BoxLayout(panneauGlobal, BoxLayout.Y_AXIS));
		
		
		// disposer mes filtres
		// TODO
		
		// TODO ici passer un truc  au lieu d'un nom de cases � cocher
		Filtre catHotel = new Filtre(FactoryFactory.getFactory(FactoryFactory.DAOFACTORY).
									getDAO(DAOFactory.DAOCATEGORIE).findAll().convertir());
		panneauGlobal.add(catHotel);
		
		//Ici, on ajoute un filtre avec les catégories de chambre
		Filtre catChambre = new Filtre(FactoryFactory.getFactory(FactoryFactory.DAOFACTORY).
				getDAO(DAOFactory.DAOTYPEHOTEL).findAll().convertir());
		panneauGlobal.add(catChambre);
				
		/*
		 * Affectation du panneau � la frame
		 * 
		 */
		
		setContentPane(panneauGlobal);		
	}
	
	public void afficher() {
		pack();
		setVisible(true); 				// afficher la fenetre
		setLocationRelativeTo(null); 	// position initiale de la fenetre = au milieu	
	}
}
