package view;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import collectionmetier.Collectionnable;
import model.Categorie;
import model.ConnexionBDD;
import model.TypeHotel;

import java.awt.BorderLayout;
import java.awt.Dimension;

public class Filtre extends JPanel {

	// Variables
	private ArrayList<JCheckBox> 		listeCheckBox 	= new ArrayList<JCheckBox>();;
	private ArrayList<Collectionnable>	maCollection	= new ArrayList<Collectionnable>();

	public Filtre() {
		setLayout(new BorderLayout(0, 0));


		// g�n�ration des cases � cocher
		for (int i = 0 ; i < 5 ; i++) {
			JCheckBox cb = new JCheckBox("cb "+i);

			//cb.setActionCommand("MonBouton1 personnalis�");

			// r�glage des param�tres du layout
			GridBagConstraints gbc = new GridBagConstraints(); 
			gbc.insets = new Insets(0,0,5,0);
			gbc.gridx = 0;
			gbc.gridy = i+1;

			// Ajout au cpanneau
			add(cb, gbc);

		}
	}	


	public Filtre(String Cat) {
		ConnexionBDD.initBDD("hotel", "hotel");

		setLayout(new GridBagLayout());

		try {
			String req="SELECT NOMCAT FROM CATEGORIE";
			//preparer la connexion
			Statement monSt=ConnexionBDD.getCxion().prepareStatement(req);
			//metre le resultat de la requete dans un ResultSet
			ResultSet monRs=monSt.executeQuery(req);

			//Je rajoute un scrollbox tant que il y a des resultat dans mon ResultSet.
			int i = 0;

			Box MaBox = Box.createVerticalBox();
			
			while(monRs.next()) {
				JCheckBox cb = new JCheckBox(monRs.getString(1));
				GridBagConstraints gbc = new GridBagConstraints(); 
				gbc.insets = new Insets(0,0,5,0);
				gbc.gridx = 0;
				gbc.gridy = i+1;
				i++;				
				// Ajout au cpanneau
				MaBox.add(cb, gbc);

			}

			JScrollPane jscrlpBox = new JScrollPane(MaBox);
			jscrlpBox.setPreferredSize(new Dimension(140, 90));
			add(jscrlpBox);
			setVisible(true);

		}catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public Filtre(ArrayList<Collectionnable> maCollection) {

		Box MaBox = Box.createVerticalBox();
		// génération des cases à cocher


		for (Collectionnable collecObjet : maCollection) {
			switch (collecObjet.getClass().getSimpleName()) {

			case "Categorie": 

				JCheckBox cb = new JCheckBox(((Categorie)collecObjet).getNomCat());
				MaBox.add(cb);	
				listeCheckBox.add(cb);
				break;

			case "TypeHotel": 

				JCheckBox cb1 = new JCheckBox(((TypeHotel)collecObjet).getNomType());
				MaBox.add(cb1);
				listeCheckBox.add(cb1);
				break;
			}
		}

		JScrollPane jscrlpBox = new JScrollPane(MaBox);
		jscrlpBox.setPreferredSize(new Dimension(140, 90));
		add(jscrlpBox);
		setVisible(true);
	}
}
