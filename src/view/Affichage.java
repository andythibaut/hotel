package view;
/**
 * 
 */

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.WindowConstants;

import factories.DAOFactory;
import factories.FactoryFactory;

import java.awt.BorderLayout;


public class Affichage extends JFrame{
	
	public Affichage() {
		
		//JFrame fenetre = new JFrame();
		setSize(1000, 800);
		setTitle("Ecran principal");
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		
		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.NORTH);
		//on va rajouter un peu de contenu. Un titre :
		JTextArea titre = new JTextArea();
		titre.setText("Gestion des hôtels");
		panel.add(titre);	
		
		JPanel panel_1 = new JPanel();
		getContentPane().add(panel_1, BorderLayout.WEST);
		
		// Ajout du filtre catégorie d'hotel 
		Filtre catHotel = new Filtre(FactoryFactory.getFactory(FactoryFactory.DAOFACTORY).
									getDAO(DAOFactory.DAOCATEGORIE).findAll().convertir());
		panel_1.add(catHotel);

		//Ici, on ajoute un filtre avec les catégories de chambre
		Filtre catChambre = new Filtre(FactoryFactory.getFactory(FactoryFactory.DAOFACTORY).
										getDAO(DAOFactory.DAOTYPEHOTEL).findAll().convertir());
		panel_1.add(catChambre);
		
		//On rajoute un bouton pour filtrer les hôtel en suivant ce qui a été coché
		//Jbutton bFiltre = new Jbutton("Rechercher");
		
		//panneau principal qui liste tous les hotels
		JPanel panel_2 = new JPanel();
		getContentPane().add(panel_2, BorderLayout.CENTER);
		// Je vais mettre tous les hotels dans une boite
		panel_2.add(new ListeFiltree());
	}

	
	public void afficher() {
		pack();
		setVisible(true); 				// afficher la fenetre
		setLocationRelativeTo(null); 	// position initiale de la fenetre = au milieu	
	}
}
